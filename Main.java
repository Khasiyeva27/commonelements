import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class Main {
    public static void main(String[] args) {

        System.out.println("Enter the length of the first array then add elements");
        Scanner sc = new Scanner(System.in);
        int[] array1 = new int[sc.nextInt()];
        for (int i = 0; i < array1.length; i++) {
            array1[i] = sc.nextInt();
        }

        System.out.println("Enter the length of the second array then add elements");
        Scanner scan = new Scanner(System.in);
        int[] array2 = new int[scan.nextInt()];
        for (int i = 0; i < array2.length; i++) {
            array2[i] = sc.nextInt();
        }

        Set<Integer> set1 = new HashSet<>(); // 1 2 4 5 6
        for (Integer element : array1) {
            set1.add(element);
        }

        Set<Integer> commonElements = new HashSet<>();
        for (Integer element : array2) {
            if (set1.contains(element)) {
                commonElements.add(element);
            }
        }

        System.out.println("Common elements:");
        for (Integer element : commonElements) {
            System.out.print(element + " ");
        }
    }
}
